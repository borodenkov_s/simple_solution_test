from django.conf.urls import patterns, include, url
from django.contrib import admin
from simple_parser.views import ParseResultListView


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'simple_solution.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', ParseResultListView.as_view(), name='parse_result_list'),
    url(r'^admin/', include(admin.site.urls)),
)

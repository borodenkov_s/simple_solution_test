# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ParseResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('http_code', models.IntegerField(null=True, verbose_name='\u041a\u043e\u0434 \u043e\u0442\u0432\u0435\u0442\u0430')),
                ('title', models.TextField(null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('create_date', models.DateField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
                'ordering': ('-create_date',),
                'verbose_name': '\u0417\u0430\u0434\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u043f\u0430\u0440\u0441\u0435\u0440\u0430',
                'verbose_name_plural': '\u0417\u0430\u0434\u0430\u043d\u0438\u044f \u0434\u043b\u044f \u043f\u0430\u0440\u0441\u0435\u0440\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ParseTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('create_date', models.DateField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
                'ordering': ('-create_date',),
                'verbose_name': '\u0417\u0430\u0434\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u043f\u0430\u0440\u0441\u0435\u0440\u0430',
                'verbose_name_plural': '\u0417\u0430\u0434\u0430\u043d\u0438\u044f \u0434\u043b\u044f \u043f\u0430\u0440\u0441\u0435\u0440\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='parseresult',
            name='task',
            field=models.ForeignKey(to='simple_parser.ParseTask'),
            preserve_default=True,
        ),
    ]

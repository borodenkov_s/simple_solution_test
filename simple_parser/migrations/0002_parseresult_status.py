# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simple_parser', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='parseresult',
            name='status',
            field=models.CharField(default=b'pending', max_length=50, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(b'pending', '\u0412 \u043e\u0447\u0435\u0440\u0435\u0434\u0438...'), (b'success', '\u0423\u0441\u043f\u0435\u0445'), (b'error', '\u041e\u0448\u0438\u0431\u043a\u0430')]),
            preserve_default=True,
        ),
    ]

from celery.task import task
from .models import ParseTask, ParseResult
import logging
from grab.spider import Spider, Task

class SimpleSpider(Spider):

    def prepare(self):

        for parse_task in ParseTask.objects.all():
            parse_result = ParseResult(task=parse_task)
            parse_result.save()
            self.add_task(Task('title', url=parse_task.url, parse_result=parse_result))

    def task_title(self, grab, task):
        parse_result = task.parse_result
        if grab.response.code == 404:
            parse_result.status = 'error'
            parse_result.http_code = grab.response.code
            parse_result.save()
            return

        title = grab.css_text('title')
        parse_result.http_code = grab.response.code
        parse_result.status = 'success'
        parse_result.title = title
        parse_result.save()

    def task_title_fallback(self, task):
        parse_result = task.parse_result
        parse_result.status = 'error'
        parse_result.save()


@task
def run_bot():
    logging.basicConfig(level=logging.DEBUG)
    bot = SimpleSpider()
    bot.run()
        
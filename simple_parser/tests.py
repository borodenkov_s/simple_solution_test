# -*- coding: utf-8 -*-

from django.test import TestCase
from factories import ParseTaskFactory, ParseResultFactory
from tasks import run_bot
from django.test.utils import override_settings
from .models import ParseResult
from django.test import Client
from django.core.urlresolvers import reverse


class ParseTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    @override_settings(CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   BROKER_BACKEND='memory')
    def test_parser(self):

        task_1 = ParseTaskFactory.create(url='http://yandex.ru/')
        task_2 = ParseTaskFactory.create(url='http://vk.com')
        task_3 = ParseTaskFactory.create(url='http://yandex.ru/xyi')
        task_4 = ParseTaskFactory.create(url='http://hostanetu1234567.com')


        run_bot.delay()

        self.assertEqual(ParseResult.objects.all().count(), 4)

        res_1 = ParseResult.objects.get(task=task_1)
        res_2 = ParseResult.objects.get(task=task_2)
        res_3 = ParseResult.objects.get(task=task_3)
        res_4 = ParseResult.objects.get(task=task_4)

        
        self.assertEqual(res_1.status, 'success')
        self.assertEqual(res_1.http_code, 200)
        self.assertEqual(res_1.title, u'Яндекс')

        
        self.assertEqual(res_2.status, 'success')
        self.assertEqual(res_2.http_code, 200)
        self.assertEqual(res_2.title, u'Welcome! | VK')

        self.assertEqual(res_3.status, 'error')
        self.assertEqual(res_3.http_code, 404)

        self.assertEqual(res_4.status, 'error')
        self.assertEqual(res_4.http_code, None)

    @override_settings(CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                   CELERY_ALWAYS_EAGER=True,
                   BROKER_BACKEND='memory')
    def test_parse_result_list(self):
        url = reverse('parse_result_list')

        ParseTaskFactory.create(url='http://yandex.ru/')
        ParseTaskFactory.create(url='http://vk.com')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 0)
        self.assertTemplateUsed(response, 'simple_parser/parse_result__list.html')

        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 2)
        self.assertTemplateUsed(response, 'simple_parser/parse_result__list.html')





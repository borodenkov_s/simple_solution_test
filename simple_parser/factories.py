import factory
from .models import ParseTask, ParseResult

class ParseTaskFactory(factory.DjangoModelFactory):

    FACTORY_FOR = ParseTask

class ParseResultFactory(factory.DjangoModelFactory):

    FACTORY_FOR = ParseResult


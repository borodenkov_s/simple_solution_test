from django.views.generic.list import ListView
from .models import ParseResult, ParseTask
from .tasks import run_bot


class ParseResultListView(ListView):

    model = ParseResult
    template_name = 'simple_parser/parse_result__list.html'

    def post(self, request):
        run_bot.delay()
        return self.get(self, request)

    def get_context_data(self,**kwargs):
        context_data = {}
        context_data.update(kwargs)
        context_data['task_count'] = ParseTask.objects.count()

        return super(ParseResultListView, self).get_context_data(**context_data)

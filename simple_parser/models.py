# -*- coding: utf-8 -*-
from django.db import models


class ParseTask(models.Model):
    """
    Description: Task for grab parser
    """
    
    url = models.URLField()
    create_date = models.DateField(u'Дата создания', auto_now=True)

    class Meta:
        verbose_name = u'Задание для парсера'
        verbose_name_plural = u'Задания для парсера'
        ordering = ('-create_date',)

    def __unicode__(self):
        return self.url

class ParseResult(models.Model):

    task = models.ForeignKey(ParseTask)
    STATUS_CHOICES = (('pending', u'В очереди...'),
        ('success', u'Успех'),
        ('error', u'Ошибка'))
    status = models.CharField(u'Статус', choices=STATUS_CHOICES, default='pending', max_length=50)
    http_code = models.IntegerField(u'Код ответа', null=True)
    title = models.TextField(u'Заголовок', null=True)
    create_date = models.DateTimeField(u'Дата создания', auto_now_add=True)

    class Meta:
        verbose_name = u'Задание для парсера'
        verbose_name_plural = u'Задания для парсера'
        ordering = ('-create_date',)

    def __unicode__(self):
        return u'{url} - {date} {status} {title}'.format(
                    url=self.task.url,
                    date=self.create_date,
                    status=self.status,
                    title=self.title
                )

